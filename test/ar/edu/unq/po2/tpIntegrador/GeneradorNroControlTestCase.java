package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;


class GeneradorNroControlTestCase {
	
	GeneradorNroControl generador;
	@BeforeEach
	public void setUpTest() {
		generador=new GeneradorNroControl();
	}
	
	@Test
	void testAsignarNroControl() {
		generador.asignarNroControl();
		assertEquals(2, generador.getNroAAsignar());
	}
	
	@Test
	void testGetNroAAsignar() {
		assertEquals(1,generador.getNroAAsignar());
	}

}
