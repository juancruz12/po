package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@SuppressWarnings("deprecation")
class ObservableSEMTestCase {

	AuxiliarObservableSEM observableSEM = new AuxiliarObservableSEM();
	AuxiliarObserverSEM observerSEM = mock(AuxiliarObserverSEM.class);
	
	
	@BeforeEach
	void setUp() {
		observableSEM.suscribir(observerSEM);
	}
	
	@Test
	void observableSEMInicializaSinObservers() {
		AuxiliarObservableSEM observableSEM = new AuxiliarObservableSEM();
		assertTrue(observableSEM.countObservers() == 0);
	}
	
	
	@Test
	void testSuscribirUnObserver() {
		assertTrue(observableSEM.countObservers() == 1);
	}
	
	@Test
	void desuscribirUnObserver() {
		observableSEM.desuscribir(observerSEM);
		assertTrue(observableSEM.countObservers() == 0);
	}
	
	@Test
	void notificarInicioEstacionamiento() {
		observableSEM.notificarInicioEstacionamiento();
		verify(observerSEM).notificacionInicioEstacionamiento();
	}
	
	@Test
	void notificarFinEstacionamiento() {
		observableSEM.notificarFinEstacionamiento();
		verify(observerSEM).notificacionFinEstacionamiento();
	}

}
