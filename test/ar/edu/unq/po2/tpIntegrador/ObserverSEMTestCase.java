package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ObserverSEMTestCase {

	AuxiliarObservableSEM observableSEM = mock(AuxiliarObservableSEM.class);
	AuxiliarObserverSEM observerSEM = new AuxiliarObserverSEM(observableSEM);
	
	
	@BeforeEach
	void setUp() {
		observableSEM.suscribir(observerSEM);
	}

	@Test
	void testGetObservable() {
		assertEquals(observerSEM.getObservableSEM(), observableSEM);
	}
	
	@Test
	void testPrintNotifiacion() {
		assertEquals(observerSEM.printNotificacion("notificacion"), "notificacion");
	}

}
