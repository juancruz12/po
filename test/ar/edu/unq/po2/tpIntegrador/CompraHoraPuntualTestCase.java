package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

class CompraHoraPuntualTestCase {

	ZonaEstacionamientoMedido zem = mock(ZonaEstacionamientoMedido.class);
	PuntoDeVenta pv = mock(PuntoDeVenta.class);
	SEM sem = mock(SEM.class);
	CompraHoraPuntual compra = new CompraHoraPuntual(1, LocalDate.now(), LocalTime.now(), "a1", 3, zem, pv);
			
	@Test
	void testActivar() {
		compra.activar(sem);
		verify(sem, times(1)).inicioEstacionamientoViaCP("a1", compra.getHora(), 3, compra, zem);
	}

	@Test
	void testGetPatente() {
		assertEquals(compra.getPatente(), "a1");
	}

	@Test
	void testSetPatente() {
		compra.setPatente("A111");
		assertEquals("A111",compra.getPatente());
	}
	
	@Test
	void testGetCantHoras() {
		assertEquals(3,compra.getCantHoras());
	}
	
	@Test
	void testSetCantHoras() {
		compra.setCantHoras(8);
		assertEquals(8,compra.getCantHoras());
	}

	@Test
	void testGetZonaEstacionamientoMedido() {
		assertEquals(compra.getZonaEstacionamientoMedido(), zem);
	}
	
}
