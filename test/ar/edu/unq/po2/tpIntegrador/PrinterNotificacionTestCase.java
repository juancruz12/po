package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import org.junit.jupiter.api.Test;

class PrinterNotificacionTestCase {

	PrinterNotificacion printer = new PrinterNotificacion();
	LocalTime mediodia = LocalTime.NOON;
	LocalTime unaYMedia = LocalTime.of(13, 30);
	String horaActualSimplificada = printer.simplificado(LocalTime.now());
	
	@Test
	void testInicioEstacionamientoALas12() {
		assertEquals(printer.inicioEstacionamiento(mediodia, mediodia),
					"Hora Inicio:12:00-----Hora M�xima:12:00");
	}
	
	@Test
	void testInicioEstacionamientoALa1yMedia() {
		assertEquals("Hora Inicio:13:30-----Hora M�xima:13:30", printer.inicioEstacionamiento(unaYMedia, unaYMedia));
	}
	
	@Test
	void testInicioEstacionamientoALaHoraActual() {
		assertEquals("Hora Inicio:" + horaActualSimplificada + "-----Hora M�xima:" + horaActualSimplificada,
				printer.inicioEstacionamiento(LocalTime.now(), LocalTime.now()));
	}
	
	@Test
	void testFinEstacionamientoALas12() {
		assertEquals("Hora Inicio:12:00-----Hora Fin:12:00Horas Estacionado:0 horas 0 minutos" + "Costo Estacionamiento:" + 0,
				printer.finEstacionamiento(mediodia, mediodia, "0 horas 0 minutos", 0));
	}
	
	@Test
	void testFinEstacionamientoALa1yMedia() {
		assertEquals("Hora Inicio:13:30-----Hora Fin:13:30Horas Estacionado:0 horas 0 minutos" + "Costo Estacionamiento:" + 0,
				printer.finEstacionamiento(unaYMedia, unaYMedia, "0 horas 0 minutos", 0));
	}
	
	@Test
	void testFinEstacionamientoALaHoraActual() {
		assertEquals("Hora Inicio:" + horaActualSimplificada + "-----Hora Fin:" + horaActualSimplificada + "Horas Estacionado:0 horas 0 minutos" + "Costo Estacionamiento:" + 0,
				printer.finEstacionamiento(LocalTime.now(), LocalTime.now(), "0 horas 0 minutos", 0));
	}
	
	@Test
	void testSimplificadoALas12() {
		assertEquals("12:00", printer.simplificado(mediodia));
	}

}
