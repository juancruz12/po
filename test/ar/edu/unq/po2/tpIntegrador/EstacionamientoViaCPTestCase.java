package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

class EstacionamientoViaCPTestCase {

	
	CompraHoraPuntual mockCompra = mock(CompraHoraPuntual.class);
	ZonaEstacionamientoMedido mockZem = mock(ZonaEstacionamientoMedido.class);
	EstacionamientoViaCP estacionamientoViaCP = new EstacionamientoViaCP("A", LocalTime.NOON, LocalTime.NOON,40, mockCompra, mockZem);
	
	@Test
	void testgetHoraMaximaDeFin() {
		assertEquals(LocalTime.NOON, estacionamientoViaCP.getHoraMaximaDeFin(LocalTime.now()));
	}
	
	@Test
	void testGetCompraHabilitante() {
		assertEquals(mockCompra, estacionamientoViaCP.getCompraHabilitante());
	}
	
	@Test
	void testSetCompraHabilitante() {
		CompraHoraPuntual mockCompra = mock(CompraHoraPuntual.class);
		estacionamientoViaCP.setCompraHabilitante(mockCompra);
		assertEquals(mockCompra, estacionamientoViaCP.getCompraHabilitante());
	}

}
