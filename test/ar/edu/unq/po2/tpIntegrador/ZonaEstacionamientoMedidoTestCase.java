package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import java.awt.Polygon;

class ZonaEstacionamientoMedidoTestCase {

	PuntoDeVenta mockPdv = mock(PuntoDeVenta.class); //dummy PuntoDeVenta
	Inspector mockInspector = mock(Inspector.class); //dummy Inspector
	Polygon mockPolygon = mock(Polygon.class); //dummy Polygon
	ZonaEstacionamientoMedido zem = new ZonaEstacionamientoMedido("A", mockInspector, mockPolygon);
	
	@Test
	void testAgregarPuntoDeVenta() {
		zem.agregarPuntoDeVenta(mockPdv);
		assertTrue(zem.getPuntosDeVenta().size() == 1);
	}
	
	@Test
	void testZEMInicializaSinPuntosDeVenta() {
		assertTrue(zem.getPuntosDeVenta().isEmpty());
	}
	
	@Test
	void testGetInspector() {
		assertEquals(zem.getInspector(), mockInspector);
	}
	
	@Test
	void testGetDescripcion() {
		assertEquals(zem.getDescripcion(), "A");
	}

}
