package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

class CompraCreditoCelularTestCase {

	PuntoDeVenta pv = mock(PuntoDeVenta.class);
	SEM sem = mock(SEM.class);
	CompraCreditoCelular compra = new CompraCreditoCelular(1, LocalDate.now(), LocalTime.now(), 15, 50, pv);
	
	@Test
	void testActivar() {
		compra.activar(sem);
		verify(sem, times(1)).recargarCelular(15, 50);
	}
	
	@Test
	void testGetNroCelular() {
		assertEquals(compra.getNroCelular(), 15);
	}
	
	@Test
	void testSetNroCelular() {
		compra.setNroCelular(456);
		assertEquals(456,compra.getNroCelular());
	}
	
	@Test
	void testGetMonto() {
		assertEquals(compra.getMonto(), 50);
	}
	
	@Test
	void testSetMonto() {
		compra.setNroCelular(111);
		assertEquals(111,compra.getNroCelular());
	}
	
}
