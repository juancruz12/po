package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InspectorTestCase {

	Inspector inspector = new Inspector("Julio");
	
	@Test
	void getNombre() {
		assertEquals("Julio", inspector.getNombre());
	}

}
