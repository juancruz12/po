package ar.edu.unq.po2.tpIntegrador;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.time.*;

class SEMTestCase {
	SEM sem;
	LocalTime horaInicio;
	LocalTime horaFin;
	
	GeneradorNroControl mockGenerador = mock(GeneradorNroControl.class); //Mock GeneradorNroControl
	Usuario mockUser = mock(Usuario.class); //Mock Usuario
	ZonaEstacionamientoMedido mockZem = mock(ZonaEstacionamientoMedido.class); //Mock ZEM
	Inspector mockInspector1 = mock(Inspector.class); //Mock Inspector 1
	Inspector mockInspector2 = mock(Inspector.class); //Mock Inspector 2
	PuntoDeVenta mockPdv = mock(PuntoDeVenta.class); //Mock PDV
	Compra mockCompra = mock(Compra.class); //Mock Compra
	
	ArrayList<PuntoDeVenta> puntosDeVenta = new ArrayList<PuntoDeVenta>();
	
	
	@BeforeEach
	public void setUpTest() {
		sem=new SEM(40,horaInicio,horaFin,mockGenerador);
		sem.registrarUsuario(123);
		sem.setInicioFranjaHoraria(LocalTime.of(07,00,00,000000000));
		sem.setFinFranjaHoraria(LocalTime.of(23,00,00,000000000));
		
		puntosDeVenta.add(mockPdv);
		when(mockZem.getPuntosDeVenta()).thenReturn(puntosDeVenta);
		
		when(mockInspector1.getNombre()).thenReturn("Jorge");
		when(mockInspector2.getNombre()).thenReturn("Jos�");
		
		when(mockUser.getNroCelular()).thenReturn(123);
		when(mockUser.getSaldo()).thenReturn((float)40);
		
		when(mockGenerador.asignarNroControl()).thenReturn(1,2,3);
		
	}
	
	public void registrarUsuarioYIniciarEstacionamientoViaApp() {
		sem.registrarUsuario(234);
		sem.recargarCelular(234, 40);
		sem.inicioEstacionamientoViaApp("123", 234, mockZem);
	}
	
	@Test
	public void agregarZEM() {
		sem.agregarZEM(mockZem);
		assertEquals(sem.getZonasEstacionamientoMedido().get(0), mockZem);
	}
	
	@Test
	public void registrarPuntoDeVenta() {
		when(mockZem.getInspector()).thenReturn(mockInspector1);
		sem.agregarZEM(mockZem);
		ZonaEstacionamientoMedido zem = sem.getZonasEstacionamientoMedido().get(0);;
		
		sem.registrarPV(mockZem, mockPdv);
		PuntoDeVenta puntoDeVenta = zem.getPuntosDeVenta().get(0);
		assertEquals(puntoDeVenta, puntosDeVenta.get(0));
	}
	
	@Test
	public void asignarInspector() {
		sem.agregarZEM(mockZem);
		sem.asignarInspector(mockInspector2, mockZem);
		Inspector i = sem.getZonasEstacionamientoMedido().get(0).getInspector();
		
		assertEquals(i, mockZem.getInspector());
	}
	
	@Test
	public void finFranjaHoraria() {
		registrarUsuarioYIniciarEstacionamientoViaApp();
		sem.finFranjaHoraria();
		assertTrue(sem.buscarEstacionamientosVigentes().isEmpty());
	}
	
	@Test
	public void consultaDeEstacionamientoVigenteNoEstaLaPatente() {
		assertFalse(sem.consultaDeEstacionamientoVigente("123"));
	}
	
	@Test
	public void consultaDeEstacionamientoVigenteNoHayEstacionamientoVigente() {
		registrarUsuarioYIniciarEstacionamientoViaApp();
		sem.finFranjaHoraria();
		assertFalse(sem.consultaDeEstacionamientoVigente("123"));
	}
	
	@Test
	public void consultaEstacionamientoVigenteHayEstacionamientoVigente() {
		registrarUsuarioYIniciarEstacionamientoViaApp();
		assertTrue(sem.consultaDeEstacionamientoVigente("123"));
	}
	
	@Test
	public void buscarEstacionamientoPorPatenteNoEstaLaPatente() {
		assertTrue(sem.buscarEstacionamientosPorPatente("").isEmpty());
	}
	
	@Test
	public void buscarEstacionamientoPorPatenteEstaLaPatente() {
		registrarUsuarioYIniciarEstacionamientoViaApp();
		assertEquals(sem.buscarEstacionamientosPorPatente("123").get(0).getPatente(), "123");
	}
	
	@Test
	public void buscarEstacionamientosVigentesNoHayEstacionamientos() {
		assertTrue(sem.buscarEstacionamientosVigentes().isEmpty());
	}
	
	@Test
	public void buscarEstacionamientosVigentesNoHayEstacionamientosVigentes() {
		registrarUsuarioYIniciarEstacionamientoViaApp();
		sem.finFranjaHoraria();
		assertTrue(sem.buscarEstacionamientosVigentes().isEmpty());
	}
	
	@Test
	public void buscarEstacionamientosVigentesHayEstacionamientosVigentes() {
		registrarUsuarioYIniciarEstacionamientoViaApp();
		assertEquals(sem.buscarEstacionamientosVigentes().get(0).getPatente(), "123");
	}
	
	@Test
	public void noHayInfraccionesAlInicializar() {
		assertTrue(sem.getInfracciones().isEmpty());
	}
	
	@Test
	public void altaDeInfraccionRegistraLaInfraccion() {
		sem.altaDeInfraccion("123", mockZem);
		assertTrue(sem.getInfracciones().size() == 1);
	}
	
	@Test
	public void asignarNroControlRetorna1LaPrimeraVez() {
		assertEquals(1,sem.asignarNroControl());
	}
	
	@Test
	public void asignarNroControlRetorna2LaSegundaVez() {
		sem.asignarNroControl();
		assertEquals(2,sem.asignarNroControl());
	}
	
	@Test
	public void registrarCompraActivaLaCompra() {
		sem.registrarCompra(mockCompra);
		verify(mockCompra).activar(sem);
	}
	
	@Test
	public void registrarCompraRegistraLaCompra() {
		sem.registrarCompra(mockCompra);
		assertEquals(mockCompra, sem.getCompras().get(0));
	}
	
	@Test
	public void recargarCelularRetornaExcepcionSiNoHayUsuario(){
		assertThrows(NullPointerException.class, () -> {sem.recargarCelular(444, 20);});
	}
	
	@Test
	public void recargarCelularCargaSaldoAlUsuario() {
		Usuario mockUser2 = mock(Usuario.class);
		when(mockUser2.getNroCelular()).thenReturn(333);
		when(mockUser2.getSaldo()).thenReturn((float)80);
		sem.registrarUsuario(333);
		sem.recargarCelular(333, 80);
		assertEquals(mockUser2.getSaldo(), sem.getUsuarios().get(1).getSaldo());
	}
	
	@Test
	public void inicioEstacionamientoViaCPRegistraUnEstacionamiento() {
		CompraHoraPuntual mockCHP = mock(CompraHoraPuntual.class);
		sem.inicioEstacionamientoViaCP("123", LocalTime.now(), (float)2, mockCHP, mockZem);
		assertTrue(sem.getEstacionamientos().size() == 1);
	}
	
	@Test
	public void inicioEstacionamientoViaCPCreaUnEstacionamientoVigente() {
		CompraHoraPuntual mockCHP = mock(CompraHoraPuntual.class);
		sem.inicioEstacionamientoViaCP("123", LocalTime.now(), (float)2, mockCHP, mockZem);
		assertTrue(sem.getEstacionamientos().get(0).estaVigente());
	}
	
	@Test
	public void consultaDeSaldoTiraExcepcionSiNoEstaElUsuario() {
		assertThrows(NullPointerException.class, () -> {sem.consultaDeSaldo(444);});
	}
	
	@Test
	public void consultaDeSaldoRetornaSaldoDelUsuario() {
		assertEquals(0, sem.consultaDeSaldo(123));
	}
	
	@Test
	public void buscarUsuarioPorNroCelularRetornaNullSiNoHayUsuario() {
		assertNull(sem.buscarUsuarioPorNroCelular(999));
	}
	
	@Test
	public void buscarUsuarioPorNroCelularRetornaElUsuario() {
		assertEquals(mockUser.getNroCelular(), sem.buscarUsuarioPorNroCelular(123).getNroCelular());
	}
	
	@Test
	public void registrarUsuarioRegistraUnNuevoUsuario() {
		sem.registrarUsuario(505);
		assertEquals(505, sem.getUsuarios().get(1).getNroCelular());
	}
	
	@Test
	public void usuarioConEstacionamientoVigenteTiraExcepcionSiNoHayUsuario() {
		assertThrows(NullPointerException.class, () -> {sem.usuarioConEstacionamientoVigente(707);});
	}
	
	@Test
	public void usuarioConEstacionamientoVigenteRetornaFalseSiNoTieneEstVigentes() {
		assertFalse(sem.usuarioConEstacionamientoVigente(123));
	}
	
	@Test
	public void usuarioConEstacionamientoVigenteRetornaTrueSiTieneEstacionamientoVigente() {
		registrarUsuarioYIniciarEstacionamientoViaApp();
		assertTrue(sem.usuarioConEstacionamientoVigente(234));
	}
	
	@Test
	public void inicioEstacionamientoViaApp() {
		String horaActual = LocalTime.now().getHour() + ":" + LocalTime.now().getMinute();
		if(horaActual.endsWith(":0")) horaActual = horaActual + 0;
		String horaMaxima = LocalTime.now().plusHours(1).getHour() + ":" + LocalTime.now().plusHours(1).getMinute();
		sem.recargarCelular(123, 40);
		
		assertEquals(sem.inicioEstacionamientoViaApp("patente", mockUser.getNroCelular(), mockZem), 
					"Hora Inicio:"+ horaActual  +"-----"+ "Hora M�xima:"+ horaMaxima);
	}
	
	@Test
	public void inicioEstacionamientoViaAppNoAlcanza() {
		sem.registrarUsuario(454);
		assertEquals(sem.inicioEstacionamientoViaApp("patente", 454, mockZem),
				"Saldo insuficiente para iniciar estacionamiento");
	}
	
	@Test
	public void inicioEstacionamientoViaAppTiraExcepcionSiNoExisteElUsuario() {
		assertThrows(NullPointerException.class, () -> {sem.inicioEstacionamientoViaApp("AD", 989, mockZem);});
	}
	
	@Test
	public void finEstacionamientoViaAppTiraExcepcionSiNoExisteElUsuario() {
		assertThrows(NullPointerException.class, () -> {sem.finEstacionamientoViaApp(989);});
	}
	
	@Test
	public void finEstacionamientoViaAppFinalizaElEstacionamiento() {
		registrarUsuarioYIniciarEstacionamientoViaApp();
		sem.finEstacionamientoViaApp(234);
		assertFalse(sem.getEstacionamientos().get(0).estaVigente());
	}
	
	@Test
	public void finEstacionamientoViaAppPrinteaElResultadoEsperado() {
		//El estacionamiento se inicia y finaliza al instante. Por ende los valores son as�.
		String duracionEst = 0 + " horas " + 0 + " minutos.";
		String horaActual = LocalTime.now().getHour() + ":" + LocalTime.now().getMinute();
		registrarUsuarioYIniciarEstacionamientoViaApp();
		
		assertEquals("Hora Inicio:" + horaActual +
		"-----Hora Fin:" + horaActual + 
		"Horas Estacionado:"+ duracionEst +
		"Costo Estacionamiento:" + 0, sem.finEstacionamientoViaApp(234));
	}
	
	@Test
	public void usuarioConSaldoSuficienteParaEstacionarTiraExcepcionSiElUsuarioNoExiste() {
		assertThrows(NullPointerException.class, () -> {sem.usuarioConSaldoSuficienteParaEstacionar(999);});
	}
	
	@Test
	public void usuarioConSaldoSuficienteParaEstacionarRetornaTrueSiTieneSaldoSuficiente() {
		sem.registrarUsuario(333);
		sem.recargarCelular(333, 40);
		assertTrue(sem.usuarioConSaldoSuficienteParaEstacionar(333));
	}
	
	@Test
	public void usuarioConSaldoSuficienteParaEstacionarRetornaFalseSiNoTieneSaldoSuficiente() {
		sem.registrarUsuario(445);
		assertFalse(sem.usuarioConSaldoSuficienteParaEstacionar(445));
	}
	
	@Test
	public void ocupacionesVaciasAlInicializar() {
		assertTrue(sem.getOcupaciones().isEmpty());
	}
	
	@Test
	public void zonasSinOcupaciones() {
		ArrayList<Integer> ocupaciones = new ArrayList<Integer>();
		ocupaciones.add(0);
		sem.agregarZEM(mockZem);
		assertEquals(sem.getOcupaciones(), ocupaciones);
	}
	
	//No pasa despu�s de las 20hs
	@Test
	public void ocupaciones() {
		sem.agregarZEM(mockZem);
		sem.registrarUsuario(123);
		sem.recargarCelular(123, 80);
		sem.inicioEstacionamientoViaApp("patente", 123, mockZem);
		assertTrue(sem.getOcupaciones().get(0) == 1);
	}
	
	@Test
	public void setInicioFranjaHoraria() {
		sem.setInicioFranjaHoraria(LocalTime.of(8,00,00,000000000));
		assertEquals("08:00", sem.getInicioFranjaHoraria().toString());
	}
	
	@Test
	public void setFinFranjaHoraria() {
		sem.setFinFranjaHoraria(LocalTime.of(21,00,00,000000000));
		assertEquals("21:00", sem.getFinFranjaHoraria().toString());
	}
	
	@Test
	public void setValorPorHora() {
		sem.setValorPorHora(0);
		assertEquals(0, sem.getValorPorHora());
	}
	
	@Test
	public void setGeneradorNroControl() {
		GeneradorNroControl mockGenerador2 = mock(GeneradorNroControl.class);
		sem.setGeneradorNroControl(mockGenerador2);
		assertEquals(mockGenerador2, sem.getGeneradorNroControl());
	}
	
	@Test
	public void getValorPorHora() {
		assertEquals(40, sem.getValorPorHora());
	}
	
	@Test
	public void getGeneradorNroControl() {
		assertEquals(mockGenerador, sem.getGeneradorNroControl());
	}
	
	@Test
	public void getInicioFranjaHoraria() {
		assertEquals("07:00", sem.getInicioFranjaHoraria().toString());
	}
	
	@Test
	public void getFinFranjaHoraria() {
		assertEquals(sem.getFinFranjaHoraria().toString(), "23:00");
	}
	
	@Test
	public void noHayEstacionamientosRegistradosAlInicializar() {
		assertTrue(sem.getEstacionamientos().isEmpty());
	}
	
	@Test
	public void noHayZonasEstacionamientoMedidoRegistradasAlInicializar() {
		assertTrue(sem.getZonasEstacionamientoMedido().isEmpty());
	}
	
	@Test
	public void noHayUsuariosRegistradosAlInicializar() {
		SEM sem2 = new SEM(40,horaInicio,horaFin,mockGenerador);
		assertTrue(sem2.getUsuarios().isEmpty());
	}
	
	@Test
	public void noHayComprasRegistradasAlInicializar() {
		assertTrue(sem.getCompras().isEmpty());
	}
	
	@Test
	public void noHayInfraccionesRegistradasAlInicializar() {
		assertTrue(sem.getInfracciones().isEmpty());
	}
	
	@Test
	public void testGetPrinter() {
		assertEquals(PrinterNotificacion.class, sem.getPrinter().getClass());
	}
	
	
}
