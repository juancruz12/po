package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.Duration;
import java.time.Instant;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RelojTestCase {

	Estacionamiento mockEst = mock(Estacionamiento.class); //Dummy Estacionamiento
	Reloj r = new Reloj(mockEst);
	
	@BeforeEach
	void setUp() {
		
	}
	
	@Test
	void duracionEsCeroAlInicializar() {
		assertEquals(r.getDuration(), Duration.ZERO);
	}
	
	@Test
	void testStart() {
		r.start();
		assertEquals(r.getStartTime().getNano(), Instant.now().getNano());
	}
	
	@Test
	void testStop() {
		r.stop();
		assertTrue(r.getEndTime().getNano() < Instant.now().getNano());
	}
	
	@Test
	void duracionEsEntreStartTimeYEndTime() {
		r.start();
		r.stop();
		assertTrue(r.getStartTime().getNano() <= r.getDuration().getNano() ||
				r.getDuration().getNano() <= r.getEndTime().getNano());
	}
	
	@Test
	void testGetEstacionamiento() {
		assertEquals(r.getEstacionamiento(), mockEst);
	}

}
