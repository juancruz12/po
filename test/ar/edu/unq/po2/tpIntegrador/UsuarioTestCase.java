package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UsuarioTestCase {

	Usuario usuario = new Usuario(345);
	
	@Test
	void testUsuarioNuevoNoTieneSaldo() {
		assertTrue(usuario.getSaldo() == 0);
	}
	
	@Test
	void testCargaCredito() {
		usuario.cargarCredito(10);
		assertTrue(usuario.getSaldo() == 10);
	}
	
	@Test
	void testDebitoSaldo() {
		usuario.cargarCredito(10);
		usuario.debitarSaldo(10);
		assertTrue(usuario.getSaldo() == 0);
	}
	
	@Test
	void testGetCelular() {
		assertTrue(usuario.getNroCelular() == 345);
	}
	
	@Test
	void testUsuarioInicializaSinEstacionamientos() {
		assertTrue(usuario.getEstacionamientos().isEmpty());
	}

}
