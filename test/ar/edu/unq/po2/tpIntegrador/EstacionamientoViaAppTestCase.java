package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class EstacionamientoViaAppTestCase {

	ZonaEstacionamientoMedido zem = mock(ZonaEstacionamientoMedido.class);
	Usuario usuario = mock(Usuario.class); //stub Usuario
	EstacionamientoViaApp est = new EstacionamientoViaApp("A3", LocalTime.now(), LocalTime.now().plusHours(1),40, zem, usuario);
	
	@Test
	void testEsVigente() {
		assertTrue(est.estaVigente());
	}
	
	@Test
	void testNoEsVigente() {
		est.finalizarEstacionamiento();
		assertFalse(est.estaVigente());
	}
	
	@Test
	void testGetPatente() {
		assertEquals(est.getPatente(), "A3");
	}
	
	@Test
	void testGetHoraInicio() {
		assertEquals(est.getHoraInicio().getHour(), LocalTime.now().getHour());
	}
	
	@Test
	void testGetHoraFin() {
		assertEquals(est.getHoraFin().getHour(), LocalTime.now().plusHours(1).getHour());
	}
	
	@Test
	void testGetUsuario() {
		assertEquals(est.getUsuario(), usuario);
	}
	
	@Test
	void testGetHoraMaximaSePasaDeLaHora() {
		when(usuario.getSaldo()).thenReturn((float)80);
		assertEquals(LocalTime.now(), est.getHoraMaximaDeFin(LocalTime.now()));
	}
	
	@Test
	void testGetHoraMaximaDeFin() {
		when(usuario.getSaldo()).thenReturn((float)40);
		assertEquals(LocalTime.now().plusHours(1).getHour(), est.getHoraMaximaDeFin(LocalTime.now().plusHours(1)).getHour());
	}
	
	

}
