package ar.edu.unq.po2.tpIntegrador;
import java.awt.Point;
import java.awt.Polygon;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

class AppUsuarioTestCase {
	SEM sem;
	SEM sem2;
	IFormaDeDesplazamiento vehicular;
	int nroCel=123;
	IModoApp automatico;
	IModoApp manual;
	Usuario mockUsuario = mock(Usuario.class);
	Point pointX;
	ZonaEstacionamientoMedido zemX= mock(ZonaEstacionamientoMedido.class);
	Point mockPoint = mock(Point.class);
	AppUsuario appUsuario= new AppUsuario(nroCel,sem, "KND001", mockPoint);
	IModoApp mockModoApp = mock(IModoApp.class);
	Polygon mockPolygon = mock(Polygon.class);
	@BeforeEach
	void setUpTest() {
		
		
		vehicular= mock(FDVehicular.class);
		sem= mock(SEM.class);
		automatico= mock(ModoAutomatico.class);
		manual = mock(ModoManual.class);
		pointX= mock(Point.class);
		ArrayList<Usuario> listUsuarios = new ArrayList<Usuario>();
		listUsuarios.add(mockUsuario);
		ArrayList<ZonaEstacionamientoMedido> zonasEstacionamientoMedido = new ArrayList<ZonaEstacionamientoMedido>();
		zonasEstacionamientoMedido.add(zemX);
		
		zemX.setArea(mockPolygon);
		when(mockPolygon.contains(mockPoint)).thenReturn(true);
		sem.agregarZEM(zemX);
		appUsuario.setSem(sem);
		appUsuario.setPuntoActual(mockPoint);
		
		//when(point);
		//when(zemX.incluyePunto(p)).thenReturn(true);
		when(mockUsuario.getNroCelular()).thenReturn(123);
		when(mockUsuario.getSaldo()).thenReturn((float)2.5);
		when(sem.getUsuarios()).thenReturn(listUsuarios);
		when(sem.inicioEstacionamientoViaApp("ASD456" ,nroCel ,zemX)).thenReturn("Estacionamiento iniciado");
		when(sem.getZonasEstacionamientoMedido()).thenReturn(zonasEstacionamientoMedido);
		// when() no es aplicable para un parámetro void
		//doNothing().when(sem.registrarUsuario(nroCel));
	}
		
	@Test 
	public void getNroCelularTest() {
		assertEquals(123,appUsuario.getNroCelular());
	}
	@Test
	public void setNroCelularTest() {
		appUsuario.setNroCelular(456);
		assertEquals(456,appUsuario.getNroCelular());
	}
	@Test
	public void getSemTest() {
		AppUsuario appUsuario = new AppUsuario(333,sem,"A30D", mockPoint);
		assertEquals(sem,appUsuario.getSem());
	}
	@Test
	public void setSemTest() {
		appUsuario.setSem(sem2);
		assertEquals(sem2,appUsuario.getSem());
	}
	@Test 
	public void registrarUsuarioTest() {
		appUsuario.registrarUsuario(123);
		assertEquals(appUsuario.getSem().getUsuarios().get(0).getNroCelular(), 123);
	}
	@Test 
	public void consultaDeSaldoTest() {
		appUsuario.registrarUsuario(123);
		appUsuario.consultaDeSaldo();
		
		assertEquals((float)2.5,appUsuario.getSem().getUsuarios().get(0).getSaldo());
	}
	@Test
	public void getModoAppTest() {
		appUsuario.setModoApp(automatico);
		assertEquals(automatico,appUsuario.getModoApp());
	}
	@Test 
	public void activarDeteccionDeDesplazamientoTest() {
		appUsuario.activarDeteccionDeDesplazamiento();
		assertEquals(true,appUsuario.getDeteccionDeDesplazamiento());
	}
	@Test
	public void getDeteccionDeDesplazamientoTest() {
		assertEquals(false,appUsuario.getDeteccionDeDesplazamiento());
	}
	@Test 
	public void desactivarModoDeDesplazamientoTest() {
		appUsuario.activarDeteccionDeDesplazamiento();
		appUsuario.desactivarDeteccionDeDesplazamiento();
		assertEquals(false,appUsuario.getDeteccionDeDesplazamiento());
	}
	@Test
	public void getFormaDeDesplazamientoTest() {
		assertEquals(appUsuario.getFormaDeDesplazamiento().getClass(),FDAPie.class);
	}
	@Test
	public void setFormaDeDesplazamientoTest() {
		//expected: ar.edu.unq.po2.tpIntegrador.FDAPie but was: FDAPie
		appUsuario.setFormaDeDesplazamiento(vehicular);
		assertEquals(appUsuario.getFormaDeDesplazamiento(),vehicular);
	}
	@Test
	public void getPatenteTest() {
		assertEquals(appUsuario.getPatente(),"KND001");
	}
	@Test
	public void setPatenteTest() {
		appUsuario.setPatente("ASD456");
		assertEquals(appUsuario.getPatente(),"ASD456");
	}
	@Test 
	public void getOrigenEstacionamientoTest() {
		assertEquals(null,appUsuario.getOrigenEstacionamiento());
	}
	@Test
	public void setOrigenEstacionamientoTest() {
		Point pointX= new Point(4,5);
		appUsuario.setOrigenEstacionamiento(pointX);
		assertEquals(pointX,appUsuario.getOrigenEstacionamiento());	
	}
	@Test 
	public void inicioEstacionamientoTest() {
		assertEquals(appUsuario.inicioEstacionamiento("ASD456"),"Estacionamiento iniciado");
	}
	@Test
	public void finEstacionamientoTest() {
		appUsuario.finEstacionamiento();
		verify(appUsuario.getSem()).finEstacionamientoViaApp(appUsuario.getNroCelular());
	}
	@Test
	public void drivingTest() {
		appUsuario.activarDeteccionDeDesplazamiento();
		appUsuario.driving();
		assertEquals(FDVehicular.class, appUsuario.getFormaDeDesplazamiento().getClass());
	}
	@Test
	public void walkingTest() {
		appUsuario.activarDeteccionDeDesplazamiento();
		appUsuario.walking();
		assertEquals(FDAPie.class, appUsuario.getFormaDeDesplazamiento().getClass());
	}
	@Test 
	public void deteccionPosibleEstacionamientoTest() {
		appUsuario.setModo(mockModoApp);
		appUsuario.deteccionPosibleEstacionamiento();
		
		verify(mockModoApp).alertaEstacionamiento(appUsuario);
	}
	@Test
	public void deteccionPosibleFinEstacionamientoTest() {
		
	}
	
	
}
