package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class FDAPieTestCase {

	FDAPie fdAPie = new FDAPie();
	AppUsuario mockApp = mock(AppUsuario.class);
	FDVehicular mockFDVehicular = mock(FDVehicular.class);
	
	@Test
	public void walkingTest() {
		fdAPie.walking(mockApp);
		verifyZeroInteractions(mockApp);
	}
	
	@Test
	public void drivingTest() {
		when(mockApp.getFormaDeDesplazamiento()).thenReturn(mockFDVehicular);
		fdAPie.driving(mockApp);
		verify(mockApp, times(1)).deteccionPosibleFinEstacionamiento();
		assertEquals(mockFDVehicular, mockApp.getFormaDeDesplazamiento());
	}
}
