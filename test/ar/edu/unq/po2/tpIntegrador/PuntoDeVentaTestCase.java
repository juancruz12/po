package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PuntoDeVentaTestCase {

	PuntoDeVenta pdv = new PuntoDeVenta("descripcion");
	
	@Test
	void getDescripcion() {
		assertEquals(pdv.getDescripcion(), "descripcion");
	}

}
