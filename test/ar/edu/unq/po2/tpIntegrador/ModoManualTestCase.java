package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.mockito.Mockito.*;

class ModoManualTestCase {
	ModoManual modoManual= new ModoManual();
	AppUsuario appUser= mock(AppUsuario.class);
	@Test
	public void alertaEstacionamientoTest() {
		assertEquals("Alerta Inicio Estacionamiento: Se encuentra ubicado en una Zona de Estacionamiento Medido, recuerde iniciar su estacionamiento",modoManual.alertaEstacionamiento(appUser));
	}
	@Test 
	public void alertaFinEstacionamietoTest() {
		assertEquals("Alerta Fin Estacionamiento: Recuerde finalizar su estacionamiento",modoManual.alertaFinEstacionamiento(appUser));
	}

}
