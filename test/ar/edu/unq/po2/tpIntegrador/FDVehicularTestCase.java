package ar.edu.unq.po2.tpIntegrador;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;

class FDVehicularTestCase {

	FDVehicular fdVehicular = new FDVehicular();
	AppUsuario mockApp = mock(AppUsuario.class);
	FDAPie mockFDAPie = mock(FDAPie.class);
	
	@Test
	public void drivingTest() {
		fdVehicular.driving(mockApp);
		verifyZeroInteractions(mockApp);
	}
	
	@Test
	public void walkingTest() {
		when(mockApp.getFormaDeDesplazamiento()).thenReturn(mockFDAPie);
		fdVehicular.walking(mockApp);
		verify(mockApp, times(1)).deteccionPosibleEstacionamiento();
		assertEquals(mockFDAPie.getClass(), mockApp.getFormaDeDesplazamiento().getClass());
	}
	

}
