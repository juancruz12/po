package ar.edu.unq.po2.tpIntegrador;

import java.awt.Point;

public interface IModoApp {

	public String alertaEstacionamiento(AppUsuario app);
	public String alertaFinEstacionamiento(AppUsuario app);
	
}
