package ar.edu.unq.po2.tpIntegrador;

public class PuntoDeVenta {

	String descripcion;
	
	public PuntoDeVenta(String descripcion) {
		setDescripcion(descripcion);
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
}
