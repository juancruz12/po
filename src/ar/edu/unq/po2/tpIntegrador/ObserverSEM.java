package ar.edu.unq.po2.tpIntegrador;

import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("deprecation")
public abstract class ObserverSEM implements Observer{

	ObservableSEM observableSEM;
	
	public ObserverSEM(ObservableSEM sem) {
		this.setObservableSEM(sem);
	}
	
	public void update(Observable obs, Object argument) {
		String arg = argument.toString();
		
		notificacion(arg);
	}
	
	public void notificacion(String notificacion) {
		switch(notificacion) {
		case "inicioEstacionamiento":
			notificacionInicioEstacionamiento();
			break;
		case "finEstacionamiento":
			notificacionFinEstacionamiento();
			break;
		}
	}
	
	public abstract void notificacionInicioEstacionamiento();
	
	public abstract void notificacionFinEstacionamiento();
	
	public void setObservableSEM(ObservableSEM sem) {
		this.observableSEM = sem;
	}
	
	public ObservableSEM getObservableSEM() {
		return observableSEM;
	}
}
