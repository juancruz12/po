package ar.edu.unq.po2.tpIntegrador;

import java.awt.Point;
import java.util.stream.Collectors;
import java.util.List;

public class AppUsuario implements MovementSensor {

	private SEM sem;
	private int nroCelular;
	private boolean deteccionDeDesplazamiento;
	private IModoApp modoApp;
	private IFormaDeDesplazamiento formaDeDesplazamiento;
	private Point origenEstacionamiento;
	private String patente;
	private Point puntoActual;
	
	public AppUsuario(int nroCelular, SEM sem, String patente, Point puntoActual) {
		this.setDeteccionDeDesplazamiento(false);
		this.setNroCelular(nroCelular);
		this.setSem(sem);
		this.setModoApp(new ModoManual());
		this.setFormaDeDesplazamiento(new FDAPie());
		this.setOrigenEstacionamiento(null);
		this.setPatente(patente);
		this.setPuntoActual(puntoActual);
		
	}
	
	public void registrarUsuario(int nroCelular) {
		this.getSem().registrarUsuario(nroCelular);
	}
	
	public String inicioEstacionamiento(String patente) {
		Point p = new Point();
		if (! this.obtenerZonaEstacionamientoMedido(p).isEmpty()) {
			ZonaEstacionamientoMedido zem = this.obtenerZonaEstacionamientoMedido(p).get(0);
			this.setOrigenEstacionamiento(p);
			return this.getSem().inicioEstacionamientoViaApp(patente, this.getNroCelular(), zem);
		}
		return "Usted no se encuentra en una Zona de Estacionamiento Medido.";
	}
	
	public String finEstacionamiento() {
		return this.getSem().finEstacionamientoViaApp(this.getNroCelular());
	}
	
	public float consultaDeSaldo() {
		return this.sem.consultaDeSaldo(this.getNroCelular());
	}
	
	public void setModo(IModoApp modoApp) {
		this.modoApp = modoApp;
	}
	
	public void activarDeteccionDeDesplazamiento() {
		this.deteccionDeDesplazamiento = true;
	}
	
	public void desactivarDeteccionDeDesplazamiento() {
		this.deteccionDeDesplazamiento = false;
	}
	
	@Override
	public void driving() {
		if (this.poseeDDActivada()) {
			this.getFormaDeDesplazamiento().driving(this);
		}
	}

	@Override
	public void walking() {
		if (this.poseeDDActivada()) {
			this.getFormaDeDesplazamiento().walking(this);
		}
	}
	
	public void deteccionPosibleEstacionamiento() {
		Point p = new Point();
		if (! (this.getSem().usuarioConEstacionamientoVigente(this.getNroCelular()) || (this.obtenerZonaEstacionamientoMedido(p)).isEmpty()) ) {
			this.getModoApp().alertaEstacionamiento(this);
		}
	}
	
	public void deteccionPosibleFinEstacionamiento() {
		Point p = new Point();
		if (this.getSem().usuarioConEstacionamientoVigente(this.getNroCelular()) && (p == this.getOrigenEstacionamiento()));
			this.getModoApp().alertaFinEstacionamiento(this);
	}

	public SEM getSem() {
		return sem;
	}

	public void setSem(SEM sem) {
		this.sem = sem;
	}

	public int getNroCelular() {
		return nroCelular;
	}

	public void setNroCelular(int nroCelular) {
		this.nroCelular = nroCelular;
	}

	public boolean getDeteccionDeDesplazamiento() {
		return deteccionDeDesplazamiento;
	}

	public IModoApp getModoApp() {
		return modoApp;
	}
	
	public Point getPuntoActual() {
		return puntoActual;
	}

	public void setModoApp(IModoApp modoApp) {
		this.modoApp = modoApp;
	}
	
	public void setPatente(String patente) {
		this.patente = patente;
	}
	
	public String getPatente() {
		return patente;
	}
	
	public void setOrigenEstacionamiento(Point p) {
		this.origenEstacionamiento = p;
	}
	
	public Point getOrigenEstacionamiento() {
		return origenEstacionamiento;
	}

	public IFormaDeDesplazamiento getFormaDeDesplazamiento() {
		return formaDeDesplazamiento;
	}
	
	public void setFormaDeDesplazamiento(IFormaDeDesplazamiento formaDeDesplazamiento) {
		this.formaDeDesplazamiento = formaDeDesplazamiento;
	}
	
	private void setDeteccionDeDesplazamiento(boolean b) {
		this.deteccionDeDesplazamiento = b;
	}
	
	public void setPuntoActual(Point p) {
		puntoActual = p;
	}

	public List<ZonaEstacionamientoMedido> obtenerZonaEstacionamientoMedido(Point p) {
		return this.getSem().getZonasEstacionamientoMedido().stream().filter(zem -> zem.incluyePunto(p) == true).collect(Collectors.toList());
	}
	
	public boolean poseeDDActivada() {
		return this.getDeteccionDeDesplazamiento();
	}
	
		
}
