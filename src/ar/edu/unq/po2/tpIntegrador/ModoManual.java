package ar.edu.unq.po2.tpIntegrador;

import java.awt.Point;

public class ModoManual implements IModoApp {

	@Override
	public String alertaEstacionamiento(AppUsuario app) {
		return ("Alerta Inicio Estacionamiento: Se encuentra ubicado en una Zona de Estacionamiento Medido, recuerde iniciar su estacionamiento");
	}

	@Override
	public String alertaFinEstacionamiento(AppUsuario app) {
		return ("Alerta Fin Estacionamiento: Recuerde finalizar su estacionamiento");
	}

}
