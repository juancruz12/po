package ar.edu.unq.po2.tpIntegrador;

import java.time.Duration;
import java.time.Instant;

public class Reloj {

	private Instant startTime;
	private Instant endTime;
	private Duration duration;
	private Estacionamiento estacionamiento;
	
	public Reloj(Estacionamiento est) {
		this.setEstacionamiento(est);
		setDuration(Duration.ZERO);
		setStartTime(Instant.now());
		setEndTime(Instant.now());
	}

	public void start() {
		setStartTime(Instant.now());
	}

	public void stop() {
		setEndTime(Instant.now());
		this.getEstacionamiento().finalizarEstacionamiento();
	}
	
	public Duration getDuration() {
		setDuration(Duration.between(getStartTime(), getEndTime()));
		return this.duration;
	}
	
	public Estacionamiento getEstacionamiento() {
		return this.estacionamiento;
	}
	
	public Instant getStartTime() {
		return this.startTime;
	}
	
	public Instant getEndTime() {
		return this.endTime;
	}
	
	private void setStartTime(Instant i) {
		this.startTime = i;
	}
	
	private void setEndTime(Instant i) {
		this.endTime = i;
	}
	
	private void setDuration(Duration d) {
		this.duration = d;
	}
	
	private void setEstacionamiento(Estacionamiento e) {
		estacionamiento = e;
	}

}
