package ar.edu.unq.po2.tpIntegrador;

import java.time.LocalDate;
import java.time.LocalTime;

public class CompraCreditoCelular extends Compra {

	private int nroCelular;
	private float monto;
	
	public CompraCreditoCelular(int nroControl, LocalDate fecha, LocalTime hora, int nroCelular, float monto, PuntoDeVenta pv) {
		super(nroControl, fecha, hora, pv);
		this.setNroCelular(nroCelular);
		this.setMonto(monto);
	
	}
	
	public void activar(SEM sem) {
		sem.recargarCelular(this.getNroCelular(), this.getMonto());
	}

	public int getNroCelular() {
		return nroCelular;
	}

	public void setNroCelular(int nroCelular) {
		this.nroCelular = nroCelular;
	}

	public float getMonto() {
		return monto;
	}

	public void setMonto(float monto) {
		this.monto = monto;
	}
	
}
