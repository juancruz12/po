package ar.edu.unq.po2.tpIntegrador;

public interface MovementSensor {

	public void driving();
	public void walking();
	
}
