package ar.edu.unq.po2.tpIntegrador;

import java.util.ArrayList;
import java.awt.Point;
import java.awt.Polygon;

public class ZonaEstacionamientoMedido {

	private String descripcion;
	private ArrayList<PuntoDeVenta> puntosDeVenta;
	private Inspector inspector;
	private Polygon area;
	
	public ZonaEstacionamientoMedido(String descripcion, Inspector inspector, Polygon poligono) {
		this.setDescripcion(descripcion);
		this.setInspector(inspector);
		this.setPuntosDeVenta(new ArrayList<PuntoDeVenta>());
		this.setArea(poligono);
	}
	
	public void agregarPuntoDeVenta(PuntoDeVenta pv) {
		this.getPuntosDeVenta().add(pv);
	}
	
	public Inspector getInspector() {
		return inspector;
	}
	
	public boolean incluyePunto(Point p) {
		return this.getArea().contains(p);
	}
	
	public ArrayList<PuntoDeVenta> getPuntosDeVenta() {
		return puntosDeVenta;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setPuntosDeVenta(ArrayList<PuntoDeVenta> puntosDeVenta) {
		this.puntosDeVenta = puntosDeVenta;
	}

	public void setInspector(Inspector inspector) {
		this.inspector = inspector;
	}
	
	public void setArea(Polygon area) {
		this.area = area;
	}
	
	public Polygon getArea() {
		return area;
	}
	
}
