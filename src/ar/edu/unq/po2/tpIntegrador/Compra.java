package ar.edu.unq.po2.tpIntegrador;
import java.time.*;

public abstract class Compra {

	private int nroControl;
	private LocalDate fecha;
	private LocalTime hora;
	private PuntoDeVenta pv;
	
	public Compra (int nroControl, LocalDate fecha, LocalTime hora, PuntoDeVenta pv) {
		this.setNroControl(nroControl);
		this.setFecha(fecha);
		this.setHora(hora);
		this.setPV(pv);
	}
	
	public abstract void activar(SEM sem);
	
	public int getNroControl() {
		return nroControl;
	}
	public void setNroControl(int nroControl) {
		this.nroControl = nroControl;
	}
	public LocalDate getFecha() {
		return fecha;
	}
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	public LocalTime getHora() {
		return hora;
	}
	public void setHora(LocalTime hora) {
		this.hora = hora;
	}
	
	public void setPV(PuntoDeVenta pv) {
		this.pv = pv;
	}
	
	public PuntoDeVenta getPV() {
		return pv;
	}
	
}

