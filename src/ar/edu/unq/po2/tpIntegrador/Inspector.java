package ar.edu.unq.po2.tpIntegrador;

public class Inspector {

	String nombre;
	
	public Inspector(String nombre) {
		this.setNombre(nombre);
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
}
