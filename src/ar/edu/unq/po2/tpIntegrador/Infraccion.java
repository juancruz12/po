package ar.edu.unq.po2.tpIntegrador;

import java.time.LocalDate;
import java.time.LocalTime;

public class Infraccion {

	private String patente;
	private Inspector inspector;
	private ZonaEstacionamientoMedido zona;
	private LocalDate fecha;
	private LocalTime hora;
	
	public Infraccion(String patente, ZonaEstacionamientoMedido zona, Inspector inspector, LocalDate fecha, LocalTime hora) {
		this.setPatente(patente);
		this.setInspector(inspector);
		this.setZona(zona);
		this.setFecha(fecha);
		this.setHora(hora);
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public void setInspector(Inspector inspector) {
		this.inspector = inspector;
	}

	public void setZona(ZonaEstacionamientoMedido zona) {
		this.zona = zona;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}
	
}
