package ar.edu.unq.po2.tpIntegrador;

public interface IFormaDeDesplazamiento {

	public void walking(AppUsuario app);
	public void driving(AppUsuario app);
	
}
