package ar.edu.unq.po2.tpIntegrador;

public class GeneradorNroControl {
	
	private int nroAAsignar;
	
	public GeneradorNroControl() {
		this.nroAAsignar = 1;
	}
	public int asignarNroControl() {
		this.nroAAsignar = this.nroAAsignar + 1;
		return (nroAAsignar - 1);
	}
	
	public int getNroAAsignar() {
		return nroAAsignar;
	}
}
