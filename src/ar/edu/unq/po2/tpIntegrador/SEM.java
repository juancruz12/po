package ar.edu.unq.po2.tpIntegrador;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.*;

public class SEM extends ObservableSEM {
	
	private int valorPorHora;
	private LocalTime inicioFranjaHoraria;
	private LocalTime finFranjaHoraria;
	private GeneradorNroControl generadorNroControl;
	private PrinterNotificacion printer;
	private List<ZonaEstacionamientoMedido> zonasEstacionamientoMedido;
	private List<Infraccion> infracciones;
	private List<Compra> compras;
	private List<Estacionamiento> estacionamientos;
	private List<Usuario> usuarios;
	
	public SEM(int valorHora,LocalTime horaInicio,LocalTime horaFin,GeneradorNroControl generador) {
		this.setValorPorHora(valorHora);
		this.setInicioFranjaHoraria(horaInicio);
		this.setFinFranjaHoraria(horaFin);
		this.setGeneradorNroControl(generador);
		this.printer = new PrinterNotificacion();
		this.zonasEstacionamientoMedido = new ArrayList<ZonaEstacionamientoMedido>();
		this.infracciones = new ArrayList<Infraccion>();
		this.compras = new ArrayList<Compra>();
		this.estacionamientos = new ArrayList<Estacionamiento>();
		this.usuarios = new ArrayList<Usuario>();
	}
	
	public void agregarZEM(ZonaEstacionamientoMedido zem) {
		this.getZonasEstacionamientoMedido().add(zem);
	}
	
	public void registrarPV(ZonaEstacionamientoMedido zem,PuntoDeVenta pv) {
		zem.agregarPuntoDeVenta(pv);
	}
	
	public void asignarInspector(Inspector inspector, ZonaEstacionamientoMedido zem) {
		zem.setInspector(inspector);
	}
	
	public void finFranjaHoraria() {
		for(Estacionamiento estacionamiento:estacionamientos) {
			estacionamiento.finalizarEstacionamiento();
		}
	}
	
	public boolean consultaDeEstacionamientoVigente(String patente) {
		return this.buscarEstacionamientosPorPatente(patente).stream().anyMatch(est -> est.estaVigente() == true);
	}
	
	public List<Estacionamiento> buscarEstacionamientosPorPatente(String patente) {
		return this.getEstacionamientos().stream().filter(est -> est.getPatente().contentEquals(patente)).collect(Collectors.toList());
	}
	
	public List<Estacionamiento> buscarEstacionamientosVigentes() {
		return this.getEstacionamientos().stream().filter(est -> est.estaVigente() == true).collect(Collectors.toList());
	}
	
	public void altaDeInfraccion(String patente, ZonaEstacionamientoMedido zona) {
		Infraccion i = new Infraccion(patente, zona, zona.getInspector(), LocalDate.now(), LocalTime.now());
		this.getInfracciones().add(i);
	}
	
	public int asignarNroControl() {
		return this.getGeneradorNroControl().asignarNroControl();
	}
	
	public void registrarCompra(Compra compra) {
		compra.activar(this);
		this.getCompras().add(compra);
	}
	
	public void recargarCelular(int celular,float monto) {
		this.buscarUsuarioPorNroCelular(celular).cargarCredito(monto);
	}
	
	public void inicioEstacionamientoViaCP(String patente, LocalTime hora, float cantHoras, CompraHoraPuntual compra, ZonaEstacionamientoMedido zem) {
		Estacionamiento e = new EstacionamientoViaCP(patente, hora, hora.plusHours((long) cantHoras), valorPorHora, compra, zem);
		this.getEstacionamientos().add(e);
	}
			
	public float consultaDeSaldo(int celular) {
		return this.buscarUsuarioPorNroCelular(celular).consultaDeSaldo();
	}
	
	public Usuario buscarUsuarioPorNroCelular(int celular) {
		for (Usuario usuario:this.getUsuarios()) {
			if (usuario.getNroCelular() == celular) {
				return usuario;
			}
		}
		return null;
	}
	
	public void registrarUsuario(int celular) { 
		this.getUsuarios().add(new Usuario(celular)); 
	}
	
	public boolean usuarioConEstacionamientoVigente(int nroCelular) {
		return this.buscarUsuarioPorNroCelular(nroCelular).hayEstacionamientoVigente();
	}
	
	public String inicioEstacionamientoViaApp(String patente, int nroCelular, ZonaEstacionamientoMedido zem) {
		if (this.usuarioConSaldoSuficienteParaEstacionar(nroCelular)) {
			EstacionamientoViaApp e = new EstacionamientoViaApp(patente, LocalTime.now(), 
														this.getFinFranjaHoraria(),
														valorPorHora,
														zem,
														this.buscarUsuarioPorNroCelular(nroCelular));
			this.buscarUsuarioPorNroCelular(nroCelular).agregarEstacionamiento(e);
			this.getEstacionamientos().add(e);
			return this.getPrinter().inicioEstacionamiento(e.getHoraInicio(), e.getHoraMaximaDeFin(this.getFinFranjaHoraria()));
		}
		return "Saldo insuficiente para iniciar estacionamiento";
	}
	
	public String finEstacionamientoViaApp(int nroCelular) {
		Usuario usuario = this.buscarUsuarioPorNroCelular(nroCelular);
		EstacionamientoViaApp est = usuario.getEstacionamientoVigente();
		est.finalizarEstacionamiento();
		usuario.debitarSaldo(est.calcularPrecio());
		
		return this.getPrinter().finEstacionamiento(est.getHoraInicio(), est.getHoraFin(), est.getHorasEstacionado(), est.calcularPrecio());
	}
	
	public boolean usuarioConSaldoSuficienteParaEstacionar(int nroCelular) {
		return (this.buscarUsuarioPorNroCelular(nroCelular).getSaldo() >= this.getValorPorHora());
	}
	
	public int getOcupacion(ZonaEstacionamientoMedido zem){
		int ocupacion = 0;
		
		for (Estacionamiento e : this.getEstacionamientos()) {
			if(e.estaVigente()) ocupacion ++;
		}
		
		return ocupacion;
	}
	
	public ArrayList<Integer> getOcupaciones(){
		ArrayList<Integer> ocupaciones = new ArrayList<Integer>();
		for(ZonaEstacionamientoMedido zem : this.getZonasEstacionamientoMedido()) {
			ocupaciones.add(getOcupacion(zem));
		}
		
		return ocupaciones;
	}
	
	public void setInicioFranjaHoraria(LocalTime horaInicio) {
		this.inicioFranjaHoraria = horaInicio;
	}
	
	public void setFinFranjaHoraria(LocalTime horaFin) {
		this.finFranjaHoraria=horaFin;
	}
	
	public void setValorPorHora(int valor) {
		this.valorPorHora = valor;
	}
	
	public void setGeneradorNroControl(GeneradorNroControl generadorNroControl) {
		this.generadorNroControl = generadorNroControl;
	}
	
	public float getValorPorHora() {
		return valorPorHora;
	}
	
	public GeneradorNroControl getGeneradorNroControl() {
		return generadorNroControl;
	}
	
	public LocalTime getInicioFranjaHoraria() {
		return inicioFranjaHoraria;
	}
	
	public LocalTime getFinFranjaHoraria() {
		return finFranjaHoraria;
	}
	
	public List<Estacionamiento> getEstacionamientos() {
		return estacionamientos;
	}
	
	public List<ZonaEstacionamientoMedido> getZonasEstacionamientoMedido() {
		return zonasEstacionamientoMedido;
	}
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	
	public List<Compra> getCompras() {
		return compras;
	}
	
	public List<Infraccion> getInfracciones() {
		return infracciones;
	}
	
	public PrinterNotificacion getPrinter() {
		return printer;
	}
	
	
}
