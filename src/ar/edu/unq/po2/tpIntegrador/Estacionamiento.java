package ar.edu.unq.po2.tpIntegrador;

import java.time.Duration;
import java.time.LocalTime;

public abstract class Estacionamiento {
	
	private String patente;
	private LocalTime horaInicio;
	private LocalTime horaFin;
	private Reloj reloj;
	private ZonaEstacionamientoMedido zem;
	private int valorPorHora;

	public Estacionamiento(String patente, LocalTime horaInicio, LocalTime horaFin, int valorPorHora, ZonaEstacionamientoMedido zem) {
		this.setPatente(patente);
		this.setHoraInicio(horaInicio);
		this.setHoraFin(horaFin);
		this.setValorPorHora(valorPorHora);
		this.setZonaEstacionamientoMedido(zem);
		this.setReloj(new Reloj(this));
		
		getReloj().start();
	}

	public boolean estaVigente() {
		int horaDeFin = getHoraFin().getHour();
		int minutosDeFin = getHoraFin().getMinute();
		return(horaDeFin > LocalTime.now().getHour() ||
				(horaDeFin == LocalTime.now().getHour() && minutosDeFin > LocalTime.now().getMinute()) );
	}

	public void finalizarEstacionamiento() {
		setHoraFin(LocalTime.now());
	}
	
	public long calcularPrecio() {
		return (horasEstacionado() / valorPorHora) + (minutosEstacionado() / (valorPorHora * 60));
	}
	
	public String getHorasEstacionado() {
		return this.horasEstacionado() + " horas " + this.minutosEstacionado() + " minutos.";
	}
	
	private long horasEstacionado() {
		return Duration.between(horaInicio, horaFin).toHours();
	}
	
	private long minutosEstacionado() {
		return Duration.between(horaInicio, horaFin).toMinutes() % 60;
	}
	
	public String getPatente() {
		return patente;
	}
	
	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}
	
	public Reloj getReloj() {
		return reloj;
	}
	
	public ZonaEstacionamientoMedido getZonaEstacionamientoMedido() {
		return zem;
	}
	
	public int getValorPorHora() {
		return valorPorHora;
	}
	
	public void setPatente(String patente) {
		this.patente = patente;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}
	
	private void setReloj(Reloj reloj) {
		this.reloj = reloj;
	}
	
	private void setZonaEstacionamientoMedido(ZonaEstacionamientoMedido zem) {
		this.zem = zem;
	}
	
	private void setValorPorHora(int valorPorHora) {
		this.valorPorHora = valorPorHora;
	}
	
	public LocalTime getHoraMaximaDeFin(LocalTime hora) {
		return horaFin;
	}
		
	
	
}
