package ar.edu.unq.po2.tpIntegrador;

import java.time.LocalTime;

public class EstacionamientoViaApp extends Estacionamiento {

	private Usuario usuario;
	
	public EstacionamientoViaApp(String patente, LocalTime horaInicio, LocalTime horaFin, int valorPorHora, ZonaEstacionamientoMedido zem, Usuario usuario) {
		super(patente, horaInicio, horaFin,valorPorHora, zem);
		setUsuario(usuario);
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	@Override
	public LocalTime getHoraMaximaDeFin(LocalTime finFranjaHoraria) {
		if (LocalTime.now().plusHours((long) hsMaximasSegunSaldo()).isAfter(finFranjaHoraria)) {
			return finFranjaHoraria;
		}
		return LocalTime.now().plusHours((long) hsMaximasSegunSaldo());
	}
	
	public float hsMaximasSegunSaldo() {
		return (this.getUsuario().getSaldo()) / this.getValorPorHora();
	}
	
}
