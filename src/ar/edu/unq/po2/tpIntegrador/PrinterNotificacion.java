package ar.edu.unq.po2.tpIntegrador;

import java.time.LocalTime;

public class PrinterNotificacion {

	public String inicioEstacionamiento(LocalTime horaInicio, LocalTime horaMaxima) {
		return "Hora Inicio:" + simplificado(horaInicio)+"-----Hora M�xima:"+ simplificado(horaMaxima);
	}
	
	public String finEstacionamiento(LocalTime horaInicio, LocalTime horaFin, String horasEstacionado, long costoEstacionamiento) {
		return "Hora Inicio:" + simplificado(horaInicio) +
		"-----Hora Fin:" + simplificado(horaFin) + 
		"Horas Estacionado:"+ horasEstacionado +
		"Costo Estacionamiento:" + costoEstacionamiento;
	}
	
	public String simplificado(LocalTime tiempo) {
		String horas = ""+ tiempo.getHour();
		String minutos = ""+tiempo.getMinute();
		
		if(minutos.equals("0")) minutos = "00"; 
		return horas + ":" + minutos;
	}
}
