package ar.edu.unq.po2.tpIntegrador;

import java.time.LocalDate;
import java.time.LocalTime;

public class CompraHoraPuntual extends Compra {
	
	private String patente;
	private float cantHoras;
	private ZonaEstacionamientoMedido zem;
	
	public CompraHoraPuntual(int nroControl, LocalDate fecha, LocalTime hora, String patente, float cantHoras, ZonaEstacionamientoMedido zem, PuntoDeVenta pv) {
		super(nroControl, fecha, hora, pv);
		this.setPatente(patente);
		this.setCantHoras(cantHoras);
		this.setZem(zem);
	}
	
	public void activar(SEM sem) {
		sem.inicioEstacionamientoViaCP(this.getPatente(), this.getHora(), this.getCantHoras(), this, this.getZonaEstacionamientoMedido());
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public float getCantHoras() {
		return cantHoras;
	}

	public void setCantHoras(float cantHoras) {
		this.cantHoras = cantHoras;
	}
	
	public ZonaEstacionamientoMedido getZonaEstacionamientoMedido() {
		return zem;
	}
	
	public void setZem(ZonaEstacionamientoMedido zem) {
		this.zem = zem;
	}

}
