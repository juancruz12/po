package ar.edu.unq.po2.tpIntegrador;

public class AuxiliarObserverSEM extends ObserverSEM{

	public AuxiliarObserverSEM(ObservableSEM observableSEM) {
		super(observableSEM);
	}
	
	public void notificacionInicioEstacionamiento() {
		printNotificacion("Se ha iniciado el estacionamiento");
	}
	
	public void notificacionFinEstacionamiento() {
		printNotificacion("Se ha iniciado el estacionamiento");
	}
	
	public String printNotificacion(String notif) {
		return notif;
	}
}
