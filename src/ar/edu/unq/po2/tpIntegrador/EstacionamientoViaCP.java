package ar.edu.unq.po2.tpIntegrador;

import java.time.LocalTime;

public class EstacionamientoViaCP extends Estacionamiento {
	
	private CompraHoraPuntual compraHabilitante;

	public EstacionamientoViaCP(String patente, LocalTime horaInicio, LocalTime horaFin, int valorPorHora, CompraHoraPuntual compra, ZonaEstacionamientoMedido zem) {
		super(patente, horaInicio, horaFin,valorPorHora, zem);
		this.setCompraHabilitante(compra);
	}

	@Override
	public LocalTime getHoraMaximaDeFin(LocalTime finFranjaHoraria) {
		return this.getHoraFin();
	}
	
	public void setCompraHabilitante(CompraHoraPuntual compra) {
		this.compraHabilitante = compra;
	}
	
	public CompraHoraPuntual getCompraHabilitante() {
		return compraHabilitante;
	}
}
