package ar.edu.unq.po2.tpIntegrador;

import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("deprecation")
public abstract class ObservableSEM extends Observable{
	
	public void suscribir(Observer o) {
		this.addObserver(o);
	}
	
	public void desuscribir(Observer o) {
		this.deleteObserver(o);
	}
	
	public void notificarInicioEstacionamiento() {
		this.hasChanged();
		this.notifyObservers("inicioEstacionamiento");
	}
	
	public void notificarFinEstacionamiento() {
		this.hasChanged();
		this.notifyObservers("finEstacionamiento");
	}
}
