package ar.edu.unq.po2.tpIntegrador;
import java.util.*;

public class Usuario {
	
	private int nroCelular;
	private float saldo;
	private List <EstacionamientoViaApp> estacionamientos;
	
	public Usuario(int celular) {
		this.setCelular(celular);
		this.setSaldo(0);
		this.estacionamientos = new ArrayList<EstacionamientoViaApp>();
	}
	
	public EstacionamientoViaApp getEstacionamientoVigente() {
		for (EstacionamientoViaApp estacionamiento: this.getEstacionamientos()) {
			if (estacionamiento.estaVigente() == true) {
				return estacionamiento;
			}
		}
		return null;
	}
	
	public void cargarCredito(float monto) {
		this.setSaldo(this.getSaldo() + monto);
	}

	public void debitarSaldo(float monto) {
		this.setSaldo(this.getSaldo() - monto);
	}
	
	public float getSaldo() {
		return saldo;
	}
	
	public int getNroCelular() {
		return nroCelular;
	}
	
	public List<EstacionamientoViaApp> getEstacionamientos(){
		return estacionamientos;
	}
	
	public void setCelular(int celular) {
		this.nroCelular = celular;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	
	public void agregarEstacionamiento(EstacionamientoViaApp estacionamiento) {
		this.getEstacionamientos().add(estacionamiento);
	}
	
	public boolean hayEstacionamientoVigente() {
		return this.getEstacionamientos().stream().anyMatch(est -> est.estaVigente() == true);
	}

	public float consultaDeSaldo() {
		if(tengoEstacionamientoVigente()) {
			return getSaldo() - this.getEstacionamientoVigente().getValorPorHora();
		} else return getSaldo();
		
	}
	
	private boolean tengoEstacionamientoVigente() {
		return !(this.getEstacionamientoVigente() == null);
	}
}
